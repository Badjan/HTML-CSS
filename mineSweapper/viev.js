var H = 800, W = 800;
function initRender() {
    var field = document.createElement("div");
    field.style.height = H + "px";
    field.style.width = W + "px";
    field.classList.add("field");
    for (var y = 0; y < ROWS; y++) {
        for (var x = 0; x < COLS; x++) {
            var block = document.createElement("div");
            block.style.height = H / ROWS + "px";
            block.style.width = W / COLS + "px";
            block.classList.add("block");
            block.id = "block_" + y + "_" + x;
            field.appendChild(block);
        }
    }
    document.body.appendChild(field);
}

initRender();

function renderField() {
    for (var y = 0; y < ROWS; y++) {
        for (var x = 0; x < COLS; x++) {
            var block = document.getElementById("block_" + y + "_" + x);
            block.classList.add("block_" + field[y][x]);
        }
    }
}

renderField();