var ROWS = 10,
    COLS = 10,
    MINES = 10;

var BLOCK_MINE = 10;

var field = [];

function inBound(y, x) {
    return (y >= 0 && y < ROWS && x >= 0 && x < COLS);
}

function generateMines() {
    var mines = [];
    for (var y = 0; y < ROWS; y++) {
        for (var x = 0; x < COLS; x++) {
            mines.push({y: y, x: x});
        }
    }
    mines.sort(function(a, b){
        return Math.random() - 0.5;
    });
    mines.length = MINES;
    for (var i = 0; i < mines.length; i++) {
        y_mine = mines[i].y;
        x_mine = mines[i].x;
        field[y_mine][x_mine] = BLOCK_MINE;
    }
}

function countMinesAround() {
    for (var y = 0; y < ROWS; y++) {
        for (var x = 0; x < COLS; x++) {
            if (field[y][x] != BLOCK_MINE) {
                var count = 0;
                for (var dy = -1; dy <= 1; dy++) {
                    for (var dx = -1; dx <= 1; dx++) {
                        var yy = y + dy;
                        var xx = x + dx;
                        if (inBound(yy, xx) && field[yy][xx] == BLOCK_MINE){
                            count++;
                        }
                    }
                }
                field[y][x] = count;
            }
        }
    }
} 

function init() {
    for (var y = 0; y < ROWS; y++) {
        field.push([]);
        for (var x = 0; x < COLS; x++) {
            field[y].push(0);
        }
    }
    generateMines();
    countMinesAround();
}

init();